# Dada una lista (lista1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]), iterarla y mostrar números divisibles por cinco, y si encuentra un número mayor que 150, detenga la iteración del bucle.

lista1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]


"""
for i in range(len(lista1)):
    if lista1[i] <= 150:
        if lista1[i]%5 == 0:
            print(lista1[i])
    else:
        break
"""

for elemento in lista1:
    if elemento <= 150:
        if elemento % 5 == 0:
            print(elemento)
    else:
        break
