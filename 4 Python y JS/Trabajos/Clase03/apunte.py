# Cadenas
cadena="algo"
import math

# Listas
listaNombres=["Harry", "Ron", "Hermione"]
print(listaNombres)
print(listaNombres[0])

listaNombres.sort()
print(listaNombres)

# Tuplas
coordenadas=(50.7, 68.23)

# Conjunto
conjunto=set()

conjunto.add("Harry")
conjunto.add("Ron")
conjunto.add("Hermione")
conjunto.add("Hermione")

conjunto.remove("Hermione")

print(len(conjunto))
print(conjunto)

# Diccionarios
casas= {"Harry":"Griffindor", "Draco":"Slytherin"}
casas ["Hermione"]="Griffindor"
print(casas)

covidData={123:True, 345:False}
covidData[367]=True
print(covidData)