### Ejercicio 2
## Escribe un programa Python que acepte 5 números decimales del usuario

# numeros=[]
# while len(numeros)<5: 
#     numeros.append(float(input("Ingrese un decimal: ")))

# # for i in range(len(numeros))

# print("Los numeros son {numeros}")

### Ejercicio 3
## Escribe un programa en Python que reciba 5 números enteros del usuario y los guarde en una lista. Luego recorrer la lista y mostrar los números por pantalla.

# lista=[]

# while len(lista)<5:
#     lista.append(int(float(input("Escriba un numero: "))))

# print("------------------------------------")

# for elemento in lista:
#     print(elemento)

### Ejercicio 4
## Dada una lista (lista1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]), iterarla y mostrar números divisibles por cinco, y si encuentra un número mayor que 150, detenga la iteración del bucle

lista1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]

for elemento in lista1:
    if elemento<=150:
        if elemento%5 == 0:
            print(elemento," es divisible por 5")
    else:
        break


