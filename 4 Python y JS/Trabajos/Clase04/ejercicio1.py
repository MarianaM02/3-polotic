# 1. Escribe una clase de Python llamada Rectangulo que se define por una longitud y un ancho y un método que calculará el área de un rectángulo.

class Rectangulo:
    def __init__(self, b, h):
        self.base=b
        self.altura=h

    def calcularArea(self):
        return self.base*self.altura

rectangulo=Rectangulo(3,5)
print(rectangulo.calcularArea())
