# 2. Crea una clase Minibus que herede de la clase Vehiculo. Debes poder tener un método capacidad() que defina por defecto la capacidad de 6 asientos. Luego crea una clase Pasajero que puedas ir agregando a una instancia de Minibus. Esa instancia no deberá permitir mas de 50 pasajeros únicos (no se permiten pasajeros repetidos)
import	sys

class Pasajero:
    def __init__(self,nombre):
        self.nombre=nombre

    def __str__(self):
        return f"{self.nombre}"
        

class Vehiculo:
    def __init__(self):
        self.listaPasajeros=set()
        self.pasajerosActuales=set()

    def subirPasajero(self,pasajero):
        if len(self.listaPasajeros)<50 and len(self.pasajerosActuales)<self.capacidadMax:
            self.listaPasajeros.add(pasajero)
            self.pasajerosActuales.add(pasajero)
        else:
            print("Lleno el cupo")
            sys.exit(1)
        
    def bajarPasajero(self, pasajero):
        if pasajero in self.pasajerosActuales:
            self.pasajerosActuales.remove(pasajero)

class Minibus(Vehiculo):
    capacidadMax=6

pasajero1=Pasajero("Laura")
pasajero2=Pasajero("NicoJ")
pasajero3=Pasajero("Euge")
bus=Minibus()

bus.subirPasajero(pasajero1)
bus.subirPasajero(pasajero2)
bus.subirPasajero(pasajero3)
bus.bajarPasajero(pasajero2)
print(pasajero1)
print(bus)
print(bus.listaPasajeros)
print(bus.pasajerosActuales)