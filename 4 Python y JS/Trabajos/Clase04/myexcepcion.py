import sys

num1=int(input("Ingrese numero 1:"))
num2=int(input("Ingrese numero 2:"))

try:
    resultado=num1/num2
except ValueError:
    print("Error. No se puede dividir por 0")
    sys.exit(1)
else:
    print(f"El resultado de dividir {num1} por {num2} es {resultado}.")

