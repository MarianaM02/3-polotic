# 2. Crea una clase Minibus que herede de la clase Vehiculo. Debes poder tener un método capacidad() que defina por defecto la capacidad de 6 asientos. Luego crea una clase Pasajero que puedas ir agregando a una instancia de Minibus. Esa instancia no deberá permitir mas de 50 pasajeros únicos (no se permiten pasajeros repetidos)

class Vehiculo():
    def __init__(self, asientos=6):
        self.asientos = asientos

    def modificarCapacidad(self, new_asientos):
        self.asientos = new_asientos

    def cargarPasajeros(self, pasajero):
        if self.disponibilidad():
            if pasajero in self.ocupados:
                print(f'{pasajero} ya esta dentro!')
            else:
                self.ocupados.append(pasajero)
        else:
            print('Se llegó al tope de la capacidad!')

    def disponibilidad(self):
        asientos = self.asientos - len(self.ocupados)
        print(f'Hay {asientos} asientos disponibles')
        return asientos

class Minibus(Vehiculo):
    def __init__(self):
        super().__init__(self)
        self.ocupados = []

class Colectivo(Vehiculo):
    pass    

class Pasajero():
    def __init__(self, nombre):
        self.nombre = nombre


auto = Vehiculo()
print(auto.asientos)

bus = Minibus()
bus.modificarCapacidad(3)  # En realidad es 50, pero es muucho para probar // estoy de acuerdo(Y)
print(bus.asientos)

while bus.disponibilidad():
    nombre = input('Como se llama el pasajero?: ')
    pasajero = Pasajero(nombre)
    bus.cargarPasajeros(pasajero.nombre)
    print(bus.ocupados)

print('Se llego al tope de la capacidad, nos vimos! 👋')