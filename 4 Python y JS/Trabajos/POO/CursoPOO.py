class Usuario:

    __edad=0
    
    def __init__(self,nombre):
        # Metodo Constructor // Initialize
        self.nombre= nombre
    
    def saludar(self):
        print("Hola mi nombre es "+ self.nombre)

    @property
    def edad(self):         #getter
        return self.__edad

    



class Empleado(Usuario):
    __salario = 0

    def modificar_salario(self,salario):
        self.__salario = salario

    def ver_salario(self):
        print(self.__salario)
        
    def saludar(self):
        super().saludar()
        print("soy un empleado")


empleado = Empleado("Uriel")
# empleado.saludar()

empleado.edad = 26