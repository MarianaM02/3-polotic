#Esta prueba consiste en escribir un pequeño programa que imprima los números del 1 al 100, pero que cuando el número sea múltiplo de 3, imprima la palabra “Fizz”; para múltiplos de 5 deberá imprimir “Buzz”, finalmente cuando el número sea múltiplo de 3 y de 5, deberá imprimir “FizzBuzz”

def fizzbuzz_1(a, b, c):
    for i in range(c):
        m = i+1 # se le agrega al indice 1 para que se muestren los num del 1 al 100
        
        if m % (a*b) == 0: # es multiplo de 3 y 5?
            print("FizzBuzz")
            continue

        if m % a == 0: # es multiplo de 3?
            print("Fizz")
            continue

        if m % b == 0: # es multiplo de 5?
            print("Buzz")
            continue

        print(m)

def esMultiplo(numero, divisor):
    return numero%divisor == 0      # 4%2 == 0   0 == 0   True
    # return not(numero%divisor)    #  not(5%2)   not(1.4)    not(True)  False  

def fizzbuzz_2 (a,b,max):
 for i in range(max) :
  numero = i+1
  respuesta = numero
  if esMultiplo (numero, a) : respuesta = "Fizz"
  if esMultiplo (numero, b) : respuesta = "Buzz"
  if esMultiplo (numero, a) and esMultiplo (numero, b) : respuesta = "FizzBuzz"
  print (respuesta) 

fizzbuzz_2(3,5,100)