from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404
from .forms import *
from .models import Articulo, Seccion
from django.utils import timezone

# from django.contrib.auth.decorators import login_required
# from django.contrib.auth.decorators import permission_required

# Create your views here.

def index(request):
    return render(request,"blog/index.html", {
        "lista_articulos": Articulo.objects.all(),
        "lista_secciones": Seccion.objects.all(),
    })

def articulo(request, articulo_id):
    un_articulo = get_object_or_404(Articulo, id=articulo_id)
    return render(request, "blog/articulo.html", {
        "articulo": un_articulo
    })

def articulo_alta(request):
    if request.method == "POST":
        user = User.objects.get(username=request.user)
        form = FormArticulo(request.POST, request.FILES, instance=Articulo(imagen=request.FILES['imagen'], publicador=user))      
        if form.is_valid():
            form.save()
            return render(request,"blog/index.html", {
                "lista_articulos": Articulo.objects.all()
            })
    else:
        form = FormArticulo(initial={'fecha_publicacion':timezone.now()})
        return render(request, "blog/articulo_nuevo.html", {
            "form": form
        })

def articulo_editar(request, articulo_id):
    un_articulo = get_object_or_404(Articulo, id=articulo_id)
    if request.method == "POST":  
        user = User.objects.get(username=request.user)   
        un_articulo.publicador = user
        form = FormArticulo(data=request.POST, files=request.FILES, instance=un_articulo)
        if form.is_valid():
            form.save()
            return render(request,"blog/index.html", {
                "lista_articulos": Articulo.objects.all()
            })
    else:
        form = FormArticulo(instance = un_articulo)
        return render(request, 'blog/articulo_edicion.html', {
            "articulo": un_articulo,
            "form": form
        })

def articulo_eliminar(request, articulo_id):
    un_articulo = get_object_or_404(Articulo, id=articulo_id)
    un_articulo.delete()
    return render(request,"blog/index.html", {
        "lista_articulos": Articulo.objects.all()
    })

def filtro_secciones(request, seccion_id):
    una_seccion = get_object_or_404(Seccion, id=seccion_id)
    queryset = Articulo.objects.all()
    queryset = queryset.filter(seccion=una_seccion)
    return render(request,"blog/index.html", {
        "lista_articulos": queryset,
        "lista_secciones": Seccion.objects.all(),
        "seccion_seleccionada": una_seccion
    })

def ver_leer_mas_tarde(request):
    pass

def add_leer_mas_tarde(request, articulo_id):
    pass

def leer_mas_tarde(request, articulo_id):
    pass