
package IGU;
import javax.swing.ImageIcon;
import Logica.ControladoraLogica;

public class Principal extends javax.swing.JFrame {
    //Se instancia un objeto con la imagen del perro
    ImageIcon imageDog = new ImageIcon("dog.png");
    //Se instancia un objeto de la clase controladora logica
    ControladoraLogica controlL = new ControladoraLogica();
    
    public Principal() {
        initComponents();
        //Seteo de la imagen en un label en la ventana
        labelPic.setIcon(imageDog);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelBody = new javax.swing.JPanel();
        panelHeader = new javax.swing.JPanel();
        separadorArriba = new javax.swing.JSeparator();
        labelTitulo = new javax.swing.JLabel();
        panelIzquierdaFormulario = new javax.swing.JPanel();
        labelNumeroCliente = new javax.swing.JLabel();
        txtNumCliente = new javax.swing.JTextField();
        labelNombreCliente = new javax.swing.JLabel();
        txtNombrePerro = new javax.swing.JTextField();
        labelRaza = new javax.swing.JLabel();
        txtRaza = new javax.swing.JTextField();
        labelColor = new javax.swing.JLabel();
        txtColor = new javax.swing.JTextField();
        labelNombreDuenio = new javax.swing.JLabel();
        txtNombreDuenio = new javax.swing.JTextField();
        labelObservaciones = new javax.swing.JLabel();
        txtCelularDuenio = new javax.swing.JTextField();
        labelCelularDuenio = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtObservaciones = new javax.swing.JTextArea();
        labelAlergico = new javax.swing.JLabel();
        labelAtencionEspecial = new javax.swing.JLabel();
        cmbAlergico = new javax.swing.JComboBox<>();
        cmbAtencionEspecial = new javax.swing.JComboBox<>();
        panelDerechaImagen = new javax.swing.JPanel();
        labelPic = new javax.swing.JLabel();
        panelBotones = new javax.swing.JPanel();
        btn_limpiar = new javax.swing.JButton();
        btn_guardar = new javax.swing.JButton();
        panelFooter = new javax.swing.JPanel();
        separadorAbajo = new javax.swing.JSeparator();
        labelNombreAlumna = new javax.swing.JLabel();
        labelTPO2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelTitulo.setFont(new java.awt.Font("Harlow Solid Italic", 1, 48)); // NOI18N
        labelTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTitulo.setText("Peluquería Canina");

        javax.swing.GroupLayout panelHeaderLayout = new javax.swing.GroupLayout(panelHeader);
        panelHeader.setLayout(panelHeaderLayout);
        panelHeaderLayout.setHorizontalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labelTitulo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(separadorArriba)
                .addContainerGap())
        );
        panelHeaderLayout.setVerticalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(labelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(separadorArriba, javax.swing.GroupLayout.DEFAULT_SIZE, 1, Short.MAX_VALUE)
                .addContainerGap())
        );

        labelNumeroCliente.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        labelNumeroCliente.setText("Cliente N°:");

        txtNumCliente.setFont(new java.awt.Font("Segoe UI Symbol", 0, 18)); // NOI18N

        labelNombreCliente.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        labelNombreCliente.setText("Nombre:");

        txtNombrePerro.setFont(new java.awt.Font("Segoe UI Symbol", 0, 18)); // NOI18N

        labelRaza.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        labelRaza.setText("Raza:");

        txtRaza.setFont(new java.awt.Font("Segoe UI Symbol", 0, 18)); // NOI18N

        labelColor.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        labelColor.setText("Color:");

        txtColor.setFont(new java.awt.Font("Segoe UI Symbol", 0, 18)); // NOI18N

        labelNombreDuenio.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        labelNombreDuenio.setText("Dueño: ");

        txtNombreDuenio.setFont(new java.awt.Font("Segoe UI Symbol", 0, 18)); // NOI18N

        labelObservaciones.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        labelObservaciones.setText("Observaciones:");

        txtCelularDuenio.setFont(new java.awt.Font("Segoe UI Symbol", 0, 18)); // NOI18N

        labelCelularDuenio.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        labelCelularDuenio.setText("Celular:");

        txtObservaciones.setColumns(20);
        txtObservaciones.setFont(new java.awt.Font("Segoe UI Symbol", 0, 18)); // NOI18N
        txtObservaciones.setRows(5);
        txtObservaciones.setMinimumSize(new java.awt.Dimension(0, 20));
        jScrollPane1.setViewportView(txtObservaciones);

        labelAlergico.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        labelAlergico.setText("Alérgico:");

        labelAtencionEspecial.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        labelAtencionEspecial.setText("Atención Especial:");

        cmbAlergico.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        cmbAlergico.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-", "SI", "NO" }));

        cmbAtencionEspecial.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        cmbAtencionEspecial.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-", "SI", "NO" }));

        javax.swing.GroupLayout panelIzquierdaFormularioLayout = new javax.swing.GroupLayout(panelIzquierdaFormulario);
        panelIzquierdaFormulario.setLayout(panelIzquierdaFormularioLayout);
        panelIzquierdaFormularioLayout.setHorizontalGroup(
            panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelIzquierdaFormularioLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelRaza)
                    .addComponent(labelObservaciones)
                    .addComponent(labelCelularDuenio)
                    .addComponent(labelNombreDuenio)
                    .addGroup(panelIzquierdaFormularioLayout.createSequentialGroup()
                        .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(panelIzquierdaFormularioLayout.createSequentialGroup()
                                    .addComponent(labelNumeroCliente)
                                    .addGap(62, 62, 62))
                                .addGroup(panelIzquierdaFormularioLayout.createSequentialGroup()
                                    .addComponent(labelNombreCliente)
                                    .addGap(74, 74, 74)))
                            .addGroup(panelIzquierdaFormularioLayout.createSequentialGroup()
                                .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelColor)
                                    .addComponent(labelAlergico)
                                    .addComponent(labelAtencionEspecial, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(15, 15, 15)))
                        .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtColor)
                            .addComponent(txtNombreDuenio)
                            .addComponent(txtCelularDuenio)
                            .addComponent(jScrollPane1)
                            .addComponent(txtNumCliente, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtNombrePerro, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtRaza, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cmbAlergico, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbAtencionEspecial, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelIzquierdaFormularioLayout.setVerticalGroup(
            panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelIzquierdaFormularioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelIzquierdaFormularioLayout.createSequentialGroup()
                        .addComponent(txtNumCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNombrePerro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtRaza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelIzquierdaFormularioLayout.createSequentialGroup()
                        .addComponent(labelNumeroCliente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelNombreCliente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelRaza)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelColor)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbAlergico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelAlergico))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbAtencionEspecial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelAtencionEspecial))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombreDuenio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelNombreDuenio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCelularDuenio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCelularDuenio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIzquierdaFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelObservaciones)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelIzquierdaFormularioLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmbAlergico, cmbAtencionEspecial, labelAlergico, labelAtencionEspecial, labelCelularDuenio, labelColor, labelNombreCliente, labelNombreDuenio, labelNumeroCliente, labelObservaciones, labelRaza, txtCelularDuenio, txtColor, txtNombreDuenio, txtNombrePerro, txtNumCliente, txtRaza});

        javax.swing.GroupLayout panelDerechaImagenLayout = new javax.swing.GroupLayout(panelDerechaImagen);
        panelDerechaImagen.setLayout(panelDerechaImagenLayout);
        panelDerechaImagenLayout.setHorizontalGroup(
            panelDerechaImagenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDerechaImagenLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(labelPic, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelDerechaImagenLayout.setVerticalGroup(
            panelDerechaImagenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDerechaImagenLayout.createSequentialGroup()
                .addComponent(labelPic, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 12, Short.MAX_VALUE))
        );

        btn_limpiar.setFont(new java.awt.Font("Segoe UI Symbol", 1, 18)); // NOI18N
        btn_limpiar.setText("Limpiar");
        btn_limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limpiarActionPerformed(evt);
            }
        });

        btn_guardar.setFont(new java.awt.Font("Segoe UI Symbol", 1, 18)); // NOI18N
        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBotonesLayout = new javax.swing.GroupLayout(panelBotones);
        panelBotones.setLayout(panelBotonesLayout);
        panelBotonesLayout.setHorizontalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(btn_limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62))
        );

        panelBotonesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_guardar, btn_limpiar});

        panelBotonesLayout.setVerticalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_limpiar)
                    .addComponent(btn_guardar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        labelNombreAlumna.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelNombreAlumna.setText("Madeira, Mariana Soledad");

        labelTPO2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelTPO2.setText("TPO Nº2 - Desarrollo Web Fullstack con Java");

        javax.swing.GroupLayout panelFooterLayout = new javax.swing.GroupLayout(panelFooter);
        panelFooter.setLayout(panelFooterLayout);
        panelFooterLayout.setHorizontalGroup(
            panelFooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFooterLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelFooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(separadorAbajo)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFooterLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(panelFooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelTPO2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelNombreAlumna, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        panelFooterLayout.setVerticalGroup(
            panelFooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFooterLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(separadorAbajo, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelNombreAlumna)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelTPO2)
                .addContainerGap())
        );

        panelFooterLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {labelNombreAlumna, labelTPO2});

        javax.swing.GroupLayout panelBodyLayout = new javax.swing.GroupLayout(panelBody);
        panelBody.setLayout(panelBodyLayout);
        panelBodyLayout.setHorizontalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBodyLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelBodyLayout.createSequentialGroup()
                        .addComponent(panelIzquierdaFormulario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelDerechaImagen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelBodyLayout.createSequentialGroup()
                        .addGroup(panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panelHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelBotones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelFooter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        panelBodyLayout.setVerticalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBodyLayout.createSequentialGroup()
                .addComponent(panelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelIzquierdaFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelDerechaImagen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelFooter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBody, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBody, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limpiarActionPerformed
        //Accionar del boton limpiar: 
        ////resetear campos, dejarlos vacios
        txtNumCliente.setText("");
        txtNombrePerro.setText("");
        txtRaza.setText("");
        txtColor.setText("");
        cmbAlergico.setSelectedIndex(0);
        cmbAtencionEspecial.setSelectedIndex(0);
        txtNombreDuenio.setText("");
        txtCelularDuenio.setText("");
        txtObservaciones.setText("");
    }//GEN-LAST:event_btn_limpiarActionPerformed

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
        //Accionar del boton guardar: 
        ////obtener informacion de los campos ingresados por el usuario y asignarlos a variables
        int numCliente = Integer.parseInt(txtNumCliente.getText());
        String nombrePerro = txtNombrePerro.getText();
        String raza = txtRaza.getText();
        String color = txtColor.getText();
        boolean alergico = ((String) cmbAlergico.getSelectedItem()).equals("SI");
        boolean atEspecial = ((String) cmbAtencionEspecial.getSelectedItem()).equals("SI");
        String nombreDuenio = txtNombreDuenio.getText();
        String celular = txtCelularDuenio.getText();
        String observaciones = txtObservaciones.getText();
        
        ////llamar al metodo de alta de la controladora en la capa logica
        ////se le pasa por parametro la informacion ingresada por el usuario
        controlL.altaCliente(numCliente, nombrePerro, raza, color, alergico, atEspecial, nombreDuenio, celular, observaciones);
        
    }//GEN-LAST:event_btn_guardarActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_guardar;
    private javax.swing.JButton btn_limpiar;
    private javax.swing.JComboBox<String> cmbAlergico;
    private javax.swing.JComboBox<String> cmbAtencionEspecial;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelAlergico;
    private javax.swing.JLabel labelAtencionEspecial;
    private javax.swing.JLabel labelCelularDuenio;
    private javax.swing.JLabel labelColor;
    private javax.swing.JLabel labelNombreAlumna;
    private javax.swing.JLabel labelNombreCliente;
    private javax.swing.JLabel labelNombreDuenio;
    private javax.swing.JLabel labelNumeroCliente;
    private javax.swing.JLabel labelObservaciones;
    private javax.swing.JLabel labelPic;
    private javax.swing.JLabel labelRaza;
    private javax.swing.JLabel labelTPO2;
    private javax.swing.JLabel labelTitulo;
    private javax.swing.JPanel panelBody;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelDerechaImagen;
    private javax.swing.JPanel panelFooter;
    private javax.swing.JPanel panelHeader;
    private javax.swing.JPanel panelIzquierdaFormulario;
    private javax.swing.JSeparator separadorAbajo;
    private javax.swing.JSeparator separadorArriba;
    private javax.swing.JTextField txtCelularDuenio;
    private javax.swing.JTextField txtColor;
    private javax.swing.JTextField txtNombreDuenio;
    private javax.swing.JTextField txtNombrePerro;
    private javax.swing.JTextField txtNumCliente;
    private javax.swing.JTextArea txtObservaciones;
    private javax.swing.JTextField txtRaza;
    // End of variables declaration//GEN-END:variables
}
