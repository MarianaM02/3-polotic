
package Persistencia;

import Logica.Cliente;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControladoraPersistencia {
    
    //Se instancia un objeto de la clase JpaController
    //usando el constructor declarado para crear la conexion con la PU
    ClienteJpaController controlJPA = new ClienteJpaController();
    
    //Metodo altaCliente que recibe el objeto que se crea en la capa Logica
    public void altaCliente(Cliente cli) {
        
        //Se utiliza el metodo create de la clase JpaController 
        //junto con un try-catch (evitar errores) para que se cree 
        //el regisro en la Base de Datos
        try {
            controlJPA.create(cli);
        } catch (Exception ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
