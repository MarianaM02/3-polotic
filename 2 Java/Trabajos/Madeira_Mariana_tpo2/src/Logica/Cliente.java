
package Logica;

import java.io.Serializable;
import javax.persistence.*;

//Creacion de clase Cliente y de mapeo de la entidad
@Entity
public class Cliente implements Serializable {
    @Id
    private int numCliente;
    @Basic
    private String nombrePerro;
    private String raza;
    private String color;
    private boolean alergico;
    private boolean atEspecial;
    private String nombreDuenio;
    private String celular;
    private String observaciones;
        
    //Constructores
    public Cliente() {
    }

    public Cliente(int numCliente, String nombrePerro, String raza, String color, boolean alergico, boolean atEspecial, String nombreDuenio, String celular, String observaciones) {
        this.numCliente = numCliente;
        this.nombrePerro = nombrePerro;
        this.raza = raza;
        this.color = color;
        this.alergico = alergico;
        this.atEspecial = atEspecial;
        this.nombreDuenio = nombreDuenio;
        this.celular = celular;
        this.observaciones = observaciones;
    }
        
    //Getters y Setters
    public int getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(int numCliente) {
        this.numCliente = numCliente;
    }

    public String getNombrePerro() {
        return nombrePerro;
    }

    public void setNombrePerro(String nombrePerro) {
        this.nombrePerro = nombrePerro;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isAlergico() {
        return alergico;
    }

    public void setAlergico(boolean alergico) {
        this.alergico = alergico;
    }

    public boolean isAtEspecial() {
        return atEspecial;
    }

    public void setAtEspecial(boolean atEspecial) {
        this.atEspecial = atEspecial;
    }

    public String getNombreDuenio() {
        return nombreDuenio;
    }

    public void setNombreDuenio(String nombreDuenio) {
        this.nombreDuenio = nombreDuenio;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
    
    
}
