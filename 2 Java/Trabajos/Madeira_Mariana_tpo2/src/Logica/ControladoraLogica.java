
package Logica;

import Persistencia.ControladoraPersistencia;

public class ControladoraLogica {
    //Se instancia un objeto de la clase controladoraPersistencia
    ControladoraPersistencia controlP = new ControladoraPersistencia();
    
    //Metodo altaCliente que recibe los parametros introducidos en la capa IGU
    public void altaCliente(int numCliente, String nombrePerro, String raza, String color, boolean alergico, boolean atEspecial, String nombreDuenio, String celular, String observaciones) {
        
        //Se instancia un objeto de la clase Cliente
        Cliente cli = new Cliente();
        
        //Seteo del objeto con la informacion que se recibe de la capa IGU
        cli.setNumCliente(numCliente);
        cli.setNombrePerro(nombrePerro);
        cli.setRaza(raza);
        cli.setColor(color);
        cli.setAlergico(alergico);
        cli.setAtEspecial(atEspecial);
        cli.setNombreDuenio(nombreDuenio);
        cli.setCelular(celular);
        cli.setObservaciones(observaciones);
        
        //Llamado del metodo de alta de la controladora en la capa Persistencia
        //se le pasa por parametro el objeto creado
        controlP.altaCliente(cli);
    }
    
    
    
}
