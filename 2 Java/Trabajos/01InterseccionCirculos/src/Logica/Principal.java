package Logica;

public class Principal {

	public static void main(String[] args) {
		//Circulo 1 y 2
		Punto p1 = new Punto(1,2);
		Circulo circ1 = new Circulo(p1, 2);
		
		Punto p2 = new Punto(5,3);
		Circulo circ2 = new Circulo(p2, 2);
		
		System.out.println(circ1.seIntersectan(circ2));
		
		//Circulo 3 y 4
		Punto p3 = new Punto(1,2);
		Circulo circ3 = new Circulo(p3, 2);
		
		Punto p4 = new Punto(5,3);
		Circulo circ4 = new Circulo(p4, 2);
		
		System.out.println(circ3.seIntersectan(circ4));
	}
	
}
