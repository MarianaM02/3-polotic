package Logica;

public class Punto {
	private double x;
	private double y;
	
	public Punto(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public Punto() {
		super();
	}

	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}


	public double distancia(Punto p) {
		return Math.sqrt(Math.pow(this.getX()-p.getX(), 2)+ Math.pow(this.getY()-p.getY(),2));
	}
}
