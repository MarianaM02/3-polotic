package Logica;

public class Circulo {
	private Punto centro;
	private int radio;
	
	public Circulo() {
		super();
	}
	
	public Circulo(Punto centro, int radio) {
		super();
		this.centro = centro;
		this.radio = radio;
	}
	
	public Punto getCentro() {
		return centro;
	}
	public void setCentro(Punto centro) {
		this.centro = centro;
	}
	public int getRadio() {
		return radio;
	}
	public void setRadio(int radio) {
		this.radio = radio;
	}
	
	
	public boolean seIntersectan(Circulo c) {
		double sumaRadios = this.getRadio()+ c.getRadio();
		double distancia = this.getCentro().distancia(c.getCentro());
		
		return distancia < sumaRadios; 
	}
	
}
