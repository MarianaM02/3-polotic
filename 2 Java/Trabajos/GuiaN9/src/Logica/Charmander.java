
package Logica;

public class Charmander extends Pokemon implements IFuego{

    public Charmander() {
    }

    public int getNumPokedex() {
        return numPokedex;
    }

    public void setNumPokedex(int numPokedex) {
        this.numPokedex = numPokedex;
    }

    public String getNombrePokemon() {
        return nombrePokemon;
    }

    public void setNombrePokemon(String nombrePokemon) {
        this.nombrePokemon = nombrePokemon;
    }

    public int getPesoPokemon() {
        return pesoPokemon;
    }

    public void setPesoPokemon(int pesoPokemon) {
        this.pesoPokemon = pesoPokemon;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getTemporadaQueAparece() {
        return temporadaQueAparece;
    }

    public void setTemporadaQueAparece(int temporadaQueAparece) {
        this.temporadaQueAparece = temporadaQueAparece;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
    
    @Override
    public void atacarPlacaje() {
        System.out.println(nombrePokemon+" ha usado Placaje");
    }

    @Override
    public void atacarArañazo() {
        System.out.println(nombrePokemon+" ha usado Arañazo");
    }

    @Override
    public void atacarMordisco() {
        System.out.println(nombrePokemon+" ha usado Mordisco");
    }

    @Override
    public void atacarPunioFuego() {
        System.out.println(nombrePokemon+" ha usado Puño de Fuego");
    }

    @Override
    public void atacarAscuas() {
        System.out.println(nombrePokemon+" ha usado Ascuas");
    }

    @Override
    public void atacarLanzallamas() {
        System.out.println(nombrePokemon+" ha usado Lanzallamas");
    }
    
}
