
package Logica;

public class PlayStation2 extends Consola{
    private String norma;
    private boolean estaChipeada;
    private int tamanioMemoryCard;

    public PlayStation2(String norma, boolean estaChipeada, int tamanioMemoryCard) {
        this.norma = norma;
        this.estaChipeada = estaChipeada;
        this.tamanioMemoryCard = tamanioMemoryCard;
    }

    public PlayStation2() {
    }

    public String getNorma() {
        return norma;
    }

    public void setNorma(String norma) {
        this.norma = norma;
    }

    public boolean isEstaChipeada() {
        return estaChipeada;
    }

    public void setEstaChipeada(boolean estaChipeada) {
        this.estaChipeada = estaChipeada;
    }

    public int getTamanioMemoryCard() {
        return tamanioMemoryCard;
    }

    public void setTamanioMemoryCard(int tamanioMemoryCard) {
        this.tamanioMemoryCard = tamanioMemoryCard;
    }

    public int getCodigoConsola() {
        return codigoConsola;
    }

    public void setCodigoConsola(int codigoConsola) {
        this.codigoConsola = codigoConsola;
    }

    @Override
    public void cargarJuego() {
        super.cargarJuego(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void leerDVD(String juego){
        System.out.println("Leyendo DVD "+juego);
    }
    public void grabarEnMemory(){
        System.out.println("Se ha guardado en MemoryCard");
    }
    
    
}
