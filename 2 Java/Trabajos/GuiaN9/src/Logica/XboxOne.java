
package Logica;

public class XboxOne extends Consola{
    private boolean estaConectadaInternet;
    private boolean activado4K;
    private boolean permiteDescargasAutomaticas;

    public XboxOne() {
    }

    public XboxOne(boolean estaConectadaInternet, boolean activado4K, boolean permiteDescargasAutomaticas) {
        this.estaConectadaInternet = estaConectadaInternet;
        this.activado4K = activado4K;
        this.permiteDescargasAutomaticas = permiteDescargasAutomaticas;
    }

    public boolean isEstaConectadaInternet() {
        return estaConectadaInternet;
    }

    public void setEstaConectadaInternet(boolean estaConectadaInternet) {
        this.estaConectadaInternet = estaConectadaInternet;
    }

    public boolean isActivado4K() {
        return activado4K;
    }

    public void setActivado4K(boolean activado4K) {
        this.activado4K = activado4K;
    }

    public boolean isPermiteDescargasAutomaticas() {
        return permiteDescargasAutomaticas;
    }

    public void setPermiteDescargasAutomaticas(boolean permiteDescargasAutomaticas) {
        this.permiteDescargasAutomaticas = permiteDescargasAutomaticas;
    }

    public int getCodigoConsola() {
        return codigoConsola;
    }

    public void setCodigoConsola(int codigoConsola) {
        this.codigoConsola = codigoConsola;
    }

    @Override
    public void cargarJuego() {
        super.cargarJuego(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void leerJuegoDigital(String juego){
        System.out.println("Leyendo "+juego+" desde tienda");
    }
}
