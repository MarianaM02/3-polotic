
package Logica;

public class Pikachu extends Pokemon implements IElectrico{

    public Pikachu() {
    }

    public int getNumPokedex() {
        return numPokedex;
    }

    public void setNumPokedex(int numPokedex) {
        this.numPokedex = numPokedex;
    }

    public String getNombrePokemon() {
        return nombrePokemon;
    }

    public void setNombrePokemon(String nombrePokemon) {
        this.nombrePokemon = nombrePokemon;
    }

    public int getPesoPokemon() {
        return pesoPokemon;
    }

    public void setPesoPokemon(int pesoPokemon) {
        this.pesoPokemon = pesoPokemon;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getTemporadaQueAparece() {
        return temporadaQueAparece;
    }

    public void setTemporadaQueAparece(int temporadaQueAparece) {
        this.temporadaQueAparece = temporadaQueAparece;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
      
    @Override
    public void atacarPlacaje() {
        System.out.println(nombrePokemon+" ha usado Placaje");
    }

    @Override
    public void atacarArañazo() {
        System.out.println(nombrePokemon+" ha usado Arañazo");
    }

    @Override
    public void atacarMordisco() {
        System.out.println(nombrePokemon+" ha usado Mordisco");
    }

    @Override
    public void atacarImpactrueno() {
        System.out.println(nombrePokemon+" ha usado Impact Trueno");
    }

    @Override
    public void atacarPunioTrueno() {
        System.out.println(nombrePokemon+" ha usado Puño Trueno");
    }

    @Override
    public void atacarRayo() {
        System.out.println(nombrePokemon+" ha usado Rayo");
    }

    @Override
    public void atacarRayoCarga() {
        System.out.println(nombrePokemon + " ha usado Rayo Carga");
    }
    
}
