
package Logica;

public class Bulbasaur extends Pokemon implements IPlanta{

    public Bulbasaur() {
    }

    public int getNumPokedex() {
        return numPokedex;
    }

    public void setNumPokedex(int numPokedex) {
        this.numPokedex = numPokedex;
    }

    public String getNombrePokemon() {
        return nombrePokemon;
    }

    public void setNombrePokemon(String nombrePokemon) {
        this.nombrePokemon = nombrePokemon;
    }

    public int getPesoPokemon() {
        return pesoPokemon;
    }

    public void setPesoPokemon(int pesoPokemon) {
        this.pesoPokemon = pesoPokemon;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getTemporadaQueAparece() {
        return temporadaQueAparece;
    }

    public void setTemporadaQueAparece(int temporadaQueAparece) {
        this.temporadaQueAparece = temporadaQueAparece;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public void atacarPlacaje() {
        System.out.println(nombrePokemon+" ha usado Placaje");
    }

    @Override
    public void atacarArañazo() {
        System.out.println(nombrePokemon+" ha usado Arañazo");
    }

    @Override
    public void atacarMordisco() {
        System.out.println(nombrePokemon+" ha usado Mordisco");
    }

    @Override
    public void atacarParalizar() {
        System.out.println(nombrePokemon+" ha usado Paralizar");
    }

    @Override
    public void atacarDrenaje() {
        System.out.println(nombrePokemon+" ha usado Drenaje");
    }

    @Override
    public void atacarHojaAfilada() {
        System.out.println(nombrePokemon+" ha usado Hoja Afilada");
    }

    @Override
    public void atacarLatigoCepa() {
        System.out.println(nombrePokemon+" ha usado Látigo Cepa");
    }
    
}
