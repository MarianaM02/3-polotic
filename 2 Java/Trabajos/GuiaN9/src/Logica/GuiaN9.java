
package Logica;

public class GuiaN9 {

    public static void main(String[] args) {
        System.out.println("Guía de Ejercicios Nº 9: Programación Orientada a Objetos - Parte 3");
        System.out.println("1) Clases abstractas");//Consolas
        PlayStation2 playStation2=new PlayStation2 ();
        Nintendo64 nintendo64=new Nintendo64 ();
        XboxOne xboxOne=new XboxOne ();
        
        playStation2.leerDVD("GTA San Andreas");
        nintendo64.leerCartucho("Mario Party 64");
        xboxOne.leerJuegoDigital("Halo");
        
        
        System.out.println(" ");
        System.out.println("2) Clases Abstractas + Interfaces");//Pokemon
        Pikachu pikachu=new Pikachu ();
        Squirtle squirtle=new Squirtle ();
        Charmander charmander=new Charmander();
        Bulbasaur bulbasaur=new Bulbasaur();
        pikachu.setNombrePokemon("Pikachu");
        bulbasaur.setNombrePokemon("Bulbasaur");
        charmander.setNombrePokemon("Charmander");
        squirtle.setNombrePokemon("Squirtle");
        
        pikachu.atacarImpactrueno();
        charmander.atacarMordisco();
        squirtle.atacarBurbuja();
        bulbasaur.atacarLatigoCepa();
    }
    
}
