
package Logica;

public class Nintendo64 extends Consola{
    private String norma;
    private boolean leePirateados;

    public Nintendo64(String norma, boolean leePirateados) {
        this.norma = norma;
        this.leePirateados = leePirateados;
    }

    public Nintendo64() {
    }

    public String getNorma() {
        return norma;
    }

    public void setNorma(String norma) {
        this.norma = norma;
    }

    public boolean isLeePirateados() {
        return leePirateados;
    }

    public void setLeePirateados(boolean leePirateados) {
        this.leePirateados = leePirateados;
    }

    public int getCodigoConsola() {
        return codigoConsola;
    }

    public void setCodigoConsola(int codigoConsola) {
        this.codigoConsola = codigoConsola;
    }

    
    public void leerCartucho(String juego){
        System.out.println("Leyendo cartucho "+juego);
    }
    @Override
    public void cargarJuego() {
        super.cargarJuego(); //To change body of generated methods, choose Tools | Templates.
    }
}
