
package Logica;

public class Squirtle extends Pokemon implements IAgua{

    public Squirtle() {
    }

    public int getNumPokedex() {
        return numPokedex;
    }

    public void setNumPokedex(int numPokedex) {
        this.numPokedex = numPokedex;
    }

    public String getNombrePokemon() {
        return nombrePokemon;
    }

    public void setNombrePokemon(String nombrePokemom) {
        this.nombrePokemon = nombrePokemom;
    }

    public int getPesoPokemon() {
        return pesoPokemon;
    }

    public void setPesoPokemon(int pesoPokemon) {
        this.pesoPokemon = pesoPokemon;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getTemporadaQueAparece() {
        return temporadaQueAparece;
    }

    public void setTemporadaQueAparece(int temporadaQueAparece) {
        this.temporadaQueAparece = temporadaQueAparece;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public void atacarPlacaje() {
        System.out.println(nombrePokemon+" ha usado Placaje");
    }

    @Override
    public void atacarArañazo() {
        System.out.println(nombrePokemon+" ha usado Arañazo");
    }

    @Override
    public void atacarMordisco() {
        System.out.println(nombrePokemon+" ha usado Mordisco");
    }

    @Override
    public void atacarHidrobomba() {
        System.out.println(nombrePokemon+" ha usado Hidrobomba");
    }

    @Override
    public void atacarPistolaAgua() {
        System.out.println(nombrePokemon+" ha usado Pistola de Agua");
    }

    @Override
    public void atacarBurbuja() {
        System.out.println(nombrePokemon+" ha usado Burbujas");
    }

    @Override
    public void atacarHidropulso() {
        System.out.println(nombrePokemon+" ha usado Hidropulso");
    }
    
}
