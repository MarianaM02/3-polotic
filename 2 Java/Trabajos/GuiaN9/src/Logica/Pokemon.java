
package Logica;

public abstract class Pokemon {
    protected int numPokedex;
    protected String nombrePokemon;
    protected int pesoPokemon;
    protected String sexo;
    protected int temporadaQueAparece;
    protected String tipo;
    
    
    
    public abstract void atacarPlacaje();
    public abstract void atacarArañazo();
    public abstract void atacarMordisco();
}
