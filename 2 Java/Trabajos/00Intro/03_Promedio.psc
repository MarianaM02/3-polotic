Algoritmo Promedio
	Definir i, cantNotas Como Entero
	Definir nota, sumatoria, prom Como Real
	sumatoria=0
	
	Escribir "Cantidad de Notas"
	Leer cantNotas
	Para i <- 1 Hasta cantNotas Con Paso 1 Hacer
		Escribir "Nota ", i
		Leer nota
		sumatoria=sumatoria+nota
	Fin Para
	
	prom = sumatoria/cantNotas
	Escribir "El Promedio es ", prom 
	
FinAlgoritmo
