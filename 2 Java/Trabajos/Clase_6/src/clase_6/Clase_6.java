//Matrices = primero FILAS segundo COLUMNAS


package clase_6;

import java.util.Scanner;

public class Clase_6 {

    public static void main(String[] args) {
        
       //Inicializar e imprimir
       
        int numeros [] = new int [8];

        for (int i=0; i<numeros.length;i++){
            numeros[i]=i;
        }
        
        for (int i=0; i<numeros.length; i++){
            System.out.println("Recorriendo indice " + i);
        }
        
        //Bidimensionales
        
        int matriz [][] = new int [4][4];
        
        Scanner teclado = new Scanner (System.in);
        
        for (int f=0; f<matriz.length;f++){
            for (int c=0; c<matriz.length;c++){
                System.out.println("Fila: "+ f +"   Columna: "+ c);
                matriz[f][c]=teclado.nextInt();
        }
        }
    }
    
}
