package GuiaN2;

import java.util.Scanner;

public class ejercicio2c {
    public static void main(String[] args) {
        //c. MARATON

        int dni, edad;
        String nombre, apellido, categoria, apellidoUpper;

        Scanner input1 = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);

        System.out.println("------------------------------");
        System.out.println("Ingrese DNI de la persona:");
        dni = input1.nextInt();
        System.out.println("Ingrese Nombre:");
        nombre = input2.next();
        
        while (dni != 0 && !nombre.equals("fin")) {
            System.out.println("Ingrese Apellido:");
            apellido = input2.next();
            System.out.println("Ingrese Edad:");
            edad = input1.nextInt();
            
            if (edad < 6){
                
                System.out.println("No puede participar de la maratón");
                
            } else {
                
                if (edad >= 6 && edad <=10){
                    categoria = "Menores A";
                } else if (edad >= 11 && edad <=17){
                    categoria = "Menores B";
                } else if (edad >= 18 && edad <=30){
                    categoria = "Juveniles";
                } else if (edad >= 31 && edad <=50){
                    categoria = "Adultos";
                } else{
                    categoria = "Adultos mayores";
                }
                
                System.out.println("");
                apellidoUpper = apellido.toUpperCase();
                System.out.println(apellidoUpper + ", " + nombre + " - Categoría: " + categoria);
                System.out.println("------------------------------");
                System.out.println("");
                
            }
            
            System.out.println("Ingrese DNI de la persona:");
            dni = input1.nextInt();
            System.out.println("Ingrese Nombre:");
            nombre = input2.next();
                
        }

    }
}
