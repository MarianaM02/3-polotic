package GuiaN2;

import java.util.Scanner;

class ejercicio2a {
    public static void main(String[] args) {
        int contador=0, dni, servicio;
        double  total;
        Scanner input = new Scanner(System.in);

        while (contador<5) {
            
            System.out.println("------------------------------");
            System.out.println("Ingrese DNI del cliente:");
            dni = input.nextInt();
            System.out.println("Seleccione el Tipo de Servicio:");
            System.out.println("    1- Internet  30 Megas");
            System.out.println("    2- Internet  50 Megas");
            System.out.println("    3- Internet 100 Megas");
            servicio = input.nextInt();
            
            switch(servicio){
                case 1:
                    total = 890*0.9;
                    break;
                case 2:
                    total = 1050*0.95;
                    break;
                case 3:
                    total = 1600;
                    break;
                default:
                    System.out.println("Servicio Inválido");
                    servicio = 0;
                    total = 0;
                    break;
            }

            System.out.println(" ");
            System.out.println("DNI: " + dni + " Monto a Pagar: $" + total + " Tipo de Servicio: " + servicio);
            System.out.println("------------------------------");
            contador++;
            
        }
    }
}