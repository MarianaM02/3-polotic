package GuiaN2;
import java.util.Scanner;

public class ejercicio1a_e {
    
    public static void main(String[] args) {
        
         //1
         for (int i=1; i<=35; i++){
             System.out.println(i);
         }
        
         //2
         int num;
         System.out.println("Ingrese un numero:");
         Scanner teclado = new Scanner (System.in);
         num = teclado.nextInt();
        
         for (int i=1; i<=num ; i++){
             System.out.println(i);
         }
        
         //3
         for (int i=200; i<=250; i=i+2){
             System.out.println(i);
         }
        
         //4
         for (int i=10; i>0; i--){
             System.out.println(i);
         }
        
        //5
        System.out.println("Palabra:");
        String palabra = teclado.next();
                
        while (!palabra.equals("salir")){
            System.out.println("La palabra ingresada fue " + palabra);
            System.out.println("Palabra:");
            palabra = teclado.next();
        }
    }
}
