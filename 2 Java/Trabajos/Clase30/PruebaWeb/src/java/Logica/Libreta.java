
package Logica;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Libreta {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idLibreta;
    @Basic
    String color;
    @OneToOne
    Alumno alu;

    public Libreta() {
    }

    public Libreta(int idLibreta, String color) {
        this.idLibreta = idLibreta;
        this.color = color;
    }

    public int getIdLibreta() {
        return idLibreta;
    }

    public void setIdLibreta(int idLibreta) {
        this.idLibreta = idLibreta;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
}
