
package Logica;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Materia {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idMateria;
    
    @Basic
    String nombre;
    String orientacion;

    public Materia() {
    }

    public Materia(int idMateria, String nombre, String orientacion) {
        this.idMateria = idMateria;
        this.nombre = nombre;
        this.orientacion = orientacion;
    }

    public int getIdMateria() {
        return idMateria;
    }

    public void setIdMateria(int idMateria) {
        this.idMateria = idMateria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrientacion() {
        return orientacion;
    }

    public void setOrientacion(String orientacion) {
        this.orientacion = orientacion;
    }
    
    
}
