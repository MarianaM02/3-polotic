
package Servlets;

import Logica.ControladoraLogica;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "SvTest", urlPatterns = {"/SvTest"})
public class SvTest extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //traigo los datos del JSP
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String colorPelo = request.getParameter("pelo");
        Boolean titulo = Boolean.parseBoolean(request.getParameter("titulo"));
        String java = request.getParameter("java");

        //traigo la sesion y asigno los atributos para abrir en cualquier JSP
        request.getSession().setAttribute("nomb", nombre);
        request.getSession().setAttribute("apel", apellido);
        request.getSession().setAttribute("colo", colorPelo);
        request.getSession().setAttribute("titu", titulo);
        request.getSession().setAttribute("java", java);
        
        //conecto con la Logica
        ControladoraLogica controlL = new ControladoraLogica();
        controlL.crearAlumno(nombre, apellido, colorPelo, titulo, java);
        
        //armar respuesta
        response.sendRedirect("pag2.jsp");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
