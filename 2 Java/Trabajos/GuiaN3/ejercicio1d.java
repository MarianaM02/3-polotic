/**
 * ejercicio4
 */
package GuiaN3;

import java.util.Scanner;
public class ejercicio1d {

    public static void main(String[] args) {
        
        double sueldos [] = new double[12];
        double sumatoria=0, promedio=0;
        Scanner teclado = new Scanner(System.in);
        System.out.println("Ingrese los sueldos del año 2020:");

        for(int i=0;i<sueldos.length;i++){
            System.out.println("Mes "+ (i+1) + ": ");
            sueldos[i]=teclado.nextDouble();
        }
        
        for(int i=0;i<sueldos.length;i++){
            sumatoria += sueldos[i];
        }
        
        promedio = sumatoria/(sueldos.length);


        System.out.println(" ");
        System.out.println("*************");
        System.out.println("Suma total de sueldos del año: " + sumatoria);
        System.out.println("Promedio de sueldos del año: " + promedio);
        System.out.println("*************");
    }
}