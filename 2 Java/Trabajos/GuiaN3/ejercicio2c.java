package GuiaN3;

import java.util.Scanner;

public class ejercicio2c {
    public static void main(String[] args) {
        
        String razas [][] = new String [3][3];
        String razaBuscada;
        Boolean seEncontro = false;

        Scanner input = new Scanner(System.in);
     
        System.out.println("Ingrese las razas:");
        
        for (int f=0; f<3; f++){
            for (int c=0; c<3; c++){
                
                razas [f][c] = input.next();
            
            }
        }

        System.out.println("----------------------------------");
        System.out.println("Raza a buscar: ('s' para salir)");
        razaBuscada = input.next();
        
        while(!razaBuscada.equals("s")){
            
            for (int f=0; f<3; f++){
                for (int c=0; c<3; c++){
                    
                    if(razaBuscada.equals(razas[f][c])){
                        
                        System.out.printf("Raza de perro encontrada en fila %d,columna %d\n", f, c);
                        System.out.println("----------------------------------");
                        seEncontro=true;
                        
                    }
                } 
            }

            if(seEncontro){
                seEncontro=false;
            }else{
                System.out.println("No se encontró la raza solicitada"); 
                System.out.println("----------------------------------");               
            }
            
            System.out.println("Raza a buscar: ('s' para salir)");
            razaBuscada = input.next();

        }
    }
}
