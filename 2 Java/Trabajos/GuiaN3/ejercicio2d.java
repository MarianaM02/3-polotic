package GuiaN3;

import java.util.Scanner;

/**
 * ejercicio2d
 */
public class ejercicio2d {

    public static void main(String[] args) {

        float boletin [][] = new float [4][4];
        Scanner input = new Scanner(System.in);
        String separador = "--------------------------";
        float suma=0;
        
        for (int f=0; f<4; f++){
            System.out.println("ALUMNO "+ (f+1)+":");
            for (int c=0; c<3; c++){
                   System.out.println("Nota "+ (c+1)+":");
                   boletin [f][c] = input.nextFloat();
                   suma += boletin [f][c];
            }
            boletin [f][3] =  suma/3;
            System.out.println(separador);
            suma=0;
        }

        //tabla
        
        System.out.println("\t\tNota 1\tNota 2\tNota 3\tPromedio");
        for (int f=0;f<4;f++) {
            System.out.print("Alumno "+ (f+1) + "\t");

            for (int c=0;c<4;c++){
                System.out.print(boletin[f][c]+"\t");
            }
            System.out.print("\n");
            
        }
    }
}
