package GuiaN3;

import java.util.Scanner;

public class ejercicio2e {
    public static void main(String[] args) {
        
        int vuelos[][] = new int [6][3];
        Scanner input = new Scanner(System.in);
        int vuelo, horario, cantPasajes;
        String seguir;


        System.out.println("INGRESE LA CANTIDAD DE ASIENTOS DISPONIBLES:");
        System.out.println("----------------------------------");
        for (int f = 0; f < 6; f++) {
            for (int c = 0; c < 3; c++) {
                System.out.println("Vuelo "+f+c+ ": ");
                vuelos[f][c] = input.nextInt();
            }
        }
        
        System.out.flush();  

        //ingreso
        System.out.println(" ");
        System.out.println("RESERVA DE PASAJES");
        System.out.println("----------------------------------");
        System.out.println("(Escriba 'next' para continuar, 'finish' para terminar)");
        seguir=input.next();
        
        while(!seguir.equals("finish")){
            System.out.println(" ");

            System.out.println("INGRESE DESTINO: ");
            System.out.println("    0 Rio de Janeiro"); 
            System.out.println("    1 Cancún");
            System.out.println("    2 Madrid");
            System.out.println("    3 Roma");
            System.out.println("    4 Milán");
            System.out.println("    5 Iguazú");
            vuelo=input.nextInt();
            System.out.println(" ");

            System.out.println("INGRESE HORARIO: ");
            System.out.println("    0 Mañana"); 
            System.out.println("    1 Mediodía");
            System.out.println("    2 Noche");
            horario=input.nextInt();
            System.out.println(" ");

            System.out.println("CANTIDAD DE PASAJES A RESERVAR: ");
            cantPasajes=input.nextInt();
            System.out.println(" ");

            if (cantPasajes>=vuelos[vuelo][horario]){
                System.out.println("Disculpe, no se pudo completar su operación dado que no hay asientos disponibles");
            }else{
                vuelos[vuelo][horario] -= cantPasajes;
                System.out.println("Su reserva fue realizada con éxito!");
            }

            System.out.println("----------------------------------");
            System.out.println("(Escriba 'next' para continuar, 'finish' para terminar)");
            seguir=input.next();
        }
    }
}
