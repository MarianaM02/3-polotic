package GuiaN3;

/**
 * ejercicio1e
 */
import java.util.Scanner;
public class ejercicio1e {

    public static void main(String[] args) {
        
        String ciudades []= new String [3];
        int tempMax [] = new int [3];
        int tempMin [] = new int [3];
        int maxAux,minAux;

        int iMax=0, iMin=0;
        Scanner input = new Scanner(System.in);

        for (int i=0;i<ciudades.length;i++){
            
            System.out.println("Ingrese el nombre de la ciudad "+ (i+1) + ": ");
            ciudades[i] = input.nextLine();
            System.out.println("Ingrese la temperatura mínima de la ciudad "+ (i+1) + ": ");
            tempMin[i] = input.nextInt();
            System.out.println("Ingrese la temperatura máxima de la ciudad "+ (i+1) + ": ");
            tempMax[i] = input.nextInt();
            input.nextLine();
            System.out.println("*************");
            
        }
        
        maxAux=tempMax[0];
        minAux=tempMin[0];

        for (int i=0;i<ciudades.length;i++){
            if(tempMax[i]>maxAux){
                maxAux=tempMax[i];
                iMax=i;
            }
            if(tempMin[i]<minAux){
                minAux=tempMin[i];
                iMin=i;
            }

        }

        System.out.println("La ciudad con la temperatura mínima más baja: "+ ciudades[iMin] + " (" + tempMin[iMin] + "°C)");
        System.out.println("La ciudad con la temperatura máxima más alta: "+ ciudades[iMax] + " (" + tempMax[iMax] + "°C)");
        System.out.println("*************");

    }
}