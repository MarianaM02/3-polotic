package GuiaN3;

import java.util.Scanner;

public class ejercicio1c {
    public static void main(String[] args) {
        int numeros []= new int [15];
        int cantDe3s = 0;
        Scanner teclado = new Scanner(System.in);

        System.out.println("Ingrese 15 numeros:");
        for(int i=0;i<numeros.length;i++){
            numeros[i]=teclado.nextInt();
        }
        
        for(int i=0;i<numeros.length;i++){
            if(numeros[i]==3){
                cantDe3s++;
            }
        }

        System.out.println(" ");
        System.out.println("*************");
        System.out.println("Cantidad de veces que el numero 3 fue ingresado: " + cantDe3s);
        System.out.println("*************");
        
    }
}
