package ClasesApuntes;
import java.util.Scanner;
public class ejercicios1 {
    
    public static void main(String[] args) {
        
        double sueldo=0;
        int categoria;
        
        System.out.println("Ingrese tipo de categoria a calcular el sueldo:");
        Scanner teclado = new Scanner (System.in);
        categoria = teclado.nextInt();
        
        if (categoria == 1) {
            sueldo = 15890 + (15890*0.10);
        } else if(categoria == 2){
            sueldo = 25630;
        } else if (categoria == 3){
            sueldo = 35560.20 - (35560.20*0.11);
        } else {
        System.out.println("Categoría inválida!");
        }
        if (categoria == 1 || categoria == 2 || categoria == 3){
            System.out.println("El sueldo para la categoría " + categoria + " es $" + sueldo);
        }
    }
    
}
