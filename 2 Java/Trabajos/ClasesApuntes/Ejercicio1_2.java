package ClasesApuntes;

import java.util.Scanner;

public class Ejercicio1_2 {

    public static void main(String[] args) {
        int num1;
        int num2;
        int aux;
        Scanner teclado = new Scanner (System.in);
        System.out.println("Ingrese Número 1:");
        num1 = teclado.nextInt();
        System.out.println("Ingrese Número 2:");
        num2 = teclado.nextInt();
        
        aux=num1;
        num1=num2;
        num2=aux;
        
        System.out.println("El Número 1 ahora es: " + num1);
        System.out.println("El Número 2 ahora es: " + num2);
    }
    
}
