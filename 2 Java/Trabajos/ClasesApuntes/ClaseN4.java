package ClasesApuntes;

import java.util.Scanner;
public class ClaseN4 {

   
    public static void main(String[] args) {
        //while con contador
        
        int cont = 0;
        
        while (cont < 5){
            System.out.println("Estoy en la vuelta " + cont);
            cont = cont + 1;
        }
        
        //while con centinela
        
        System.out.println("Ingrese un nombre");
        Scanner teclado = new Scanner (System.in);
        String nombre = teclado.next();
                
        while (!nombre.equals("Mariana")){
            System.out.println("El nombre ingresado fue " + nombre);
            nombre = teclado.next();
        }
        
        //for
        
        for (int i=0; i<5; i++) {
            System.out.println("Estoy en la vuelta " + i);
        
        }
        
    }
    
}
