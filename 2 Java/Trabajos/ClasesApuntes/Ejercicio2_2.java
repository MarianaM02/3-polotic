package ClasesApuntes;

import java.util.Scanner;

public class Ejercicio2_2 {

    public static void main(String[] args) {
       int cantidad_paquetes;
        System.out.println("Cantidad de paquetes a comprar:");
       Scanner teclado = new Scanner (System.in);
       cantidad_paquetes = teclado.nextInt();
       
       if (cantidad_paquetes < 5){
           System.out.println("No se permiten pedidos menores a 5 paquetes.");
       } else if (cantidad_paquetes >= 5 && cantidad_paquetes <= 15){
           System.out.println("El costo de envío $200.");
       } else if (cantidad_paquetes > 15){
           System.out.println("El envío es gratuito.");
       }
    }
    
}
