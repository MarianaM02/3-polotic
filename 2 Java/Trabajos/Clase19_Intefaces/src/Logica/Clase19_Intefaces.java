
package Logica;

public class Clase19_Intefaces {

    public static void main(String[] args) {
        
        Mono mono = new Mono();
        Pato pato = new Pato();
        Tiburon tiburon = new Tiburon();
        Gorrion gorrion = new Gorrion();
        
        
        System.out.println("-----MONO-----");
        mono.soyAnimal();
        System.out.println("-----PATO-----");
        pato.soyAnimal();
        pato.volar();
        pato.nadar();
        System.out.println("-----TIBURON-----");
        tiburon.soyAnimal();
        tiburon.nadar();
        System.out.println("-----GORRION-----");
        gorrion.soyAnimal();
        gorrion.volar();
        
        
    }
    
}
