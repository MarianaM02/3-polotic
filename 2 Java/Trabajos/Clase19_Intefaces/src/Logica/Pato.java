
package Logica;

public class Pato extends Animal implements IVolador,IAcuatico{
    
    private int cantKmNadar;
    private int cantKmCaminar;

    
    
    @Override
    public void soyAnimal() {
        System.out.println("Soy un animal, soy un pato");
    }

    @Override
    public void volar() {
        System.out.println("Hola soy un pato y vuelo al ras del agua");

    }

    @Override
    public void nadar() {
        System.out.println("Hola soy un pato y nado sobre el agua");

    }
    
}
