
package Logica;

import IGU.Interfaz;


public class Main {
   
    public static void main(String[] args) {
        Interfaz ventana = new Interfaz();
        
        ventana.setVisible(true);
        ventana.setLocationRelativeTo(null);
    }
    
}
