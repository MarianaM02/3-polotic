
package Logica;

public class Colectivo extends Vehiculo{
    private boolean aptoDiscapacitados;
    private boolean poseeLectorSube;
    private String tipoColectivo;

    public Colectivo(boolean aptoDiscapacitados, boolean poseeLectorSube, String tipoColectivo, String patente, String chasis, String motor, String color, String marca, String modelo, int cantAsientos) {
        super(patente, chasis, motor, color, marca, modelo, cantAsientos);
        this.aptoDiscapacitados = aptoDiscapacitados;
        this.poseeLectorSube = poseeLectorSube;
        this.tipoColectivo = tipoColectivo;
    }

    

    @Override
    protected String mensaje() {
        return super.mensaje()+ "un colectivo y mi cantidad de asientos es: "+ this.getCantAsientos(); //To change body of generated methods, choose Tools | Templates.
    }
    
    //Getters y Setters
    public boolean isAptoDiscapacitados() {
        return aptoDiscapacitados;
    }

    public void setAptoDiscapacitados(boolean aptoDiscapacitados) {
        this.aptoDiscapacitados = aptoDiscapacitados;
    }

    public boolean isPoseeLectorSube() {
        return poseeLectorSube;
    }

    public void setPoseeLectorSube(boolean poseeLectorSube) {
        this.poseeLectorSube = poseeLectorSube;
    }

    public String getTipoColectivo() {
        return tipoColectivo;
    }

    public void setTipoColectivo(String tipoColectivo) {
        this.tipoColectivo = tipoColectivo;
    }

    
    
       
}
