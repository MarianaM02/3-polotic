
package Logica;

public class Camion extends Vehiculo{
    private boolean tieneAcoplado;
    private int cantEjes;

    public Camion(boolean tieneAcoplado, int cantEjes, String patente, String chasis, String motor, String color, String marca, String modelo, int cantAsientos) {
        super(patente, chasis, motor, color, marca, modelo, cantAsientos);
        this.tieneAcoplado = tieneAcoplado;
        this.cantEjes = cantEjes;
    }

    

    @Override
    protected String mensaje() {
        return super.mensaje()+ "un camión y mi cantidad de ejes es: "+ this.getCantEjes(); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    //Getters y Setters
    public boolean isTieneAcoplado() {
        return tieneAcoplado;
    }

    public void setTieneAcoplado(boolean tieneAcoplado) {
        this.tieneAcoplado = tieneAcoplado;
    }

    public int getCantEjes() {
        return cantEjes;
    }

    public void setCantEjes(int cantEjes) {
        this.cantEjes = cantEjes;
    }
 
    
    
}
