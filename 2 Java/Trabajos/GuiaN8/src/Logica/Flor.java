/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

/**
 *
 * @author monyx
 */
class Flor extends Planta{
    private String colorPetalos;
    private int cantidadPromedioPetalos;
    private String colorPistilo;
    private String variedad;
    private String estacion;

    public Flor(String nombre,
            double altoTallo,
            boolean tieneHojas,
            String climaIdeal,
            String colorPetalos,
            int cantidadPromedioPetalos,
            String colorPistilo,
            String variedad,
            String estacion
            ) {
        super(nombre,
                altoTallo,
                tieneHojas,
                climaIdeal);
        this.colorPetalos = colorPetalos;
        this.cantidadPromedioPetalos = cantidadPromedioPetalos;
        this.colorPistilo = colorPistilo;
        this.variedad = variedad;
        this.estacion = estacion;
    }

    @Override
    protected String mensaje() {
        return super.mensaje()+"una flor"; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    //Getters y Setters
    public String getColorPetalos() {
        return colorPetalos;
    }

    public void setColorPetalos(String colorPetalos) {
        this.colorPetalos = colorPetalos;
    }

    public int getCantidadPromedioPetalos() {
        return cantidadPromedioPetalos;
    }

    public void setCantidadPromedioPetalos(int cantidadPromedioPetalos) {
        this.cantidadPromedioPetalos = cantidadPromedioPetalos;
    }

    public String getColorPistilo() {
        return colorPistilo;
    }

    public void setColorPistilo(String colorPistilo) {
        this.colorPistilo = colorPistilo;
    }

    public String getVariedad() {
        return variedad;
    }

    public void setVariedad(String variedad) {
        this.variedad = variedad;
    }

    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }
}
