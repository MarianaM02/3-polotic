
package Logica;

public class Auto extends Vehiculo{
    private String materialAsientos;
    private double cantidadCaballos;

    public Auto(String materialAsientos, double cantidadCaballos, String patente, String chasis, String motor, String color, String marca, String modelo, int cantAsientos) {
        super(patente, chasis, motor, color, marca, modelo, cantAsientos);
        this.materialAsientos = materialAsientos;
        this.cantidadCaballos = cantidadCaballos;
    }

    
    @Override
    protected String mensaje() {
        return super.mensaje() + "un auto y mi marca es: "+ this.getMarca(); //To change body of generated methods, choose Tools | Templates.
    }

    
    //Getters y Setters
    public String getMaterialAsientos() {
        return materialAsientos;
    }

    public void setMaterialAsientos(String materialAsientos) {
        this.materialAsientos = materialAsientos;
    }

    public double getCantidadCaballos() {
        return cantidadCaballos;
    }

    public void setCantidadCaballos(double cantidadCaballos) {
        this.cantidadCaballos = cantidadCaballos;
    }
    
    
    
}
