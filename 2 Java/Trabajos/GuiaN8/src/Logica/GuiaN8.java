
package Logica;

public class GuiaN8 {

    public static void main(String[] args) {
        
        //Repaso Objetos 
        //Ejercicio 1
        System.out.println("Repaso Objetos");
        System.out.println("Ejercicio 1");
        System.out.println("");
        VideoJuego vector[]= new VideoJuego[5];
        VideoJuego juego1= new VideoJuego(001, "Portal", "PC", 1, "Puzzle");
        VideoJuego juego2= new VideoJuego(002, "Portal2", "PC", 2, "Puzzle");
        VideoJuego juego3= new VideoJuego(003, "Stardew Valley", "PC", 4, "Casual");
        VideoJuego juego4= new VideoJuego(004, "Minecraft", "PC", 1, "Sandbox");
        VideoJuego juego5= new VideoJuego(005, "Mario Kart 64", "Nintendo 64", 4, "Carreras");
        
        vector[0]=juego1; 
        vector[1]=juego2; 
        vector[2]=juego3; 
        vector[3]=juego4; 
        vector[4]=juego5;
        
        System.out.println("Impresión del vector con todos los juegos:");
        for (int i = 0; i < vector.length; i++) {
            System.out.println("Título: "+ vector[i].getTitulo()+ ", Consola: "+vector[i].getConsola()+ ", Cantidad de jugadores: "+vector[i].getCantidadJugadores());  
        }
        
        juego2.setCantidadJugadores(4);
        juego3.setCantidadJugadores(6);

        System.out.println("");
        System.out.println("Impresión del vector modificado:");
        for (int i = 0; i < vector.length; i++) {
            System.out.println("Título: "+ vector[i].getTitulo()+ ", Consola: "+vector[i].getConsola()+ ", Cantidad de jugadores: "+vector[i].getCantidadJugadores());  
        }

        System.out.println("");
        System.out.println("Impresión de los juegos de la consola Nintendo64:");
        for (int i = 0; i < vector.length; i++) {
            if(vector[i].getConsola().equals("Nintendo 64")){
            System.out.println("Título: "+ vector[i].getTitulo()+ ", Consola: "+vector[i].getConsola()+ ", Cantidad de jugadores: "+vector[i].getCantidadJugadores());  
            }
        }
        
        System.out.println("");
        //Herencia, Polimorfismo y encapsulamiento
        //Ejercicio 1
        System.out.println("Herencia, Polimorfismo y encapsulamiento");
        System.out.println("Ejercicio 1");
        System.out.println("");
        
        
        
        Arbol arbolito= new Arbol("sauce","leñoso",0.3,"verde","alargadas","Sauce Electrico", 1.5,true,"templado");
        Flor florcita= new Flor("Dalia",0.3,true,"frio","blanco",50,"amarillo","flor","primavera");
        Arbusto arbustito= new Arbusto("Rosal",0.50,true,"templado",0.5,true,"floral","verde",true);
        
        System.out.println("Mensajes de las plantas:");
        System.out.println(arbolito.mensaje());
        System.out.println(florcita.mensaje());
        System.out.println(arbustito.mensaje());
        System.out.println("");
        
        System.out.println("Ejercicio 2");
        System.out.println("");
        
        Auto auto1=new Auto("cuero", 400, "khf879", "hisfl658", "lkjf87", "blanco", "Renault", "logan", 5);
        Auto auto2=new Auto("tela", 400, "khf879", "hisfl658", "lkjf87", "blanco", "Renault", "18", 5);
        Auto auto3=new Auto("cuero", 400, "khf879", "hisfl658", "lkjf87", "azul", "Peugeot", "405", 5);
        
        Moto moto1=new Moto(150, "acero", "kfhdsla78", "jhfa98", "khkf87", "negro", "BMW", "897", 2);
        Moto moto2=new Moto(220, "acero", "kfhdsla78", "jhfa98", "khkf87", "negro", "BMW", "897", 2);
        Moto moto3=new Moto(250, "acero", "kfhdsla78", "jhfa98", "khkf87", "negro", "BMW", "897", 2);
        
        Colectivo colectivo1=new Colectivo(true, true, "corta distancia", "gfjsdk87", "khfjad8795", "hkdu56", "azul", "Volkswagen", "gjkl", 20);
        Colectivo colectivo2=new Colectivo(true, false, "larga distancia", "jhf8790", "hhfaskjhf89", "kfjlk78", "violeta", "Volkswagen", "lkd890", 50);
        
        Camion camion1=new Camion(true, 4, "jhfsd689", "567289454dghj", "esmkhb8", "rojo", "DAF", "CF", 2);
        
        Vehiculo vectorVehiculo[] = new Vehiculo[9];
        
        vectorVehiculo[0]=auto1;
        vectorVehiculo[1]=auto2;
        vectorVehiculo[2]=auto3;
        vectorVehiculo[3]=moto1;
        vectorVehiculo[4]=moto2;
        vectorVehiculo[5]=moto3;
        vectorVehiculo[6]=colectivo1;
        vectorVehiculo[7]=colectivo2;
        vectorVehiculo[8]=camion1;
        
        System.out.println("Mensajes de los Vehiculos:");
        for (int i = 0; i < vectorVehiculo.length; i++) {
            System.out.println(vectorVehiculo[i].mensaje());
        }
        
        
    }
    
}
