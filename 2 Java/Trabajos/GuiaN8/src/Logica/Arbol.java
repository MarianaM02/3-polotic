/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

/**
 *
 * @author monyx
 */
class Arbol extends Planta{
    private String variedad;
    private String tipoTronco;
    private double radioTronco;
    private String color;
    private String tipoHojas;

    /**
     *
     * @param variedad
     * @param tipoTronco
     * @param radioTronco
     * @param color
     * @param tipoHojas
     * @param nombre
     * @param altoTallo
     * @param tieneHojas
     * @param climaIdeal
     */
    public Arbol(String variedad,
            String tipoTronco,
            double radioTronco,
            String color,
            String tipoHojas,
            String nombre,
            double altoTallo,
            boolean tieneHojas,
            String climaIdeal) {
        super(nombre,
                altoTallo,
                tieneHojas,
                climaIdeal);
        this.variedad = variedad;
        this.tipoTronco = tipoTronco;
        this.radioTronco = radioTronco;
        this.color = color;
        this.tipoHojas = tipoHojas;
    }

    @Override
    protected String mensaje() {
        return super.mensaje()+"un árbol"; //To change body of generated methods, choose Tools | Templates.
    }
    
    //getters y setters
    public String getVariedad() {
        return variedad;
    }

    public void setVariedad(String variedad) {
        this.variedad = variedad;
    }

    public String getTipoTronco() {
        return tipoTronco;
    }

    public void setTipoTronco(String tipoTronco) {
        this.tipoTronco = tipoTronco;
    }

    public double getRadioTronco() {
        return radioTronco;
    }

    public void setRadioTronco(double radioTronco) {
        this.radioTronco = radioTronco;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipoHojas() {
        return tipoHojas;
    }

    public void setTipoHojas(String tipoHojas) {
        this.tipoHojas = tipoHojas;
    }
}
