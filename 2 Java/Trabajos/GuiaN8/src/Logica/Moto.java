
package Logica;

public class Moto extends Vehiculo{
    private int cilindrada;
    private String materialManubrio;

    public Moto(int cilindrada, String materialManubrio, String patente, String chasis, String motor, String color, String marca, String modelo, int cantAsientos) {
        super(patente, chasis, motor, color, marca, modelo, cantAsientos);
        this.cilindrada = cilindrada;
        this.materialManubrio = materialManubrio;
    }

    

    @Override
    protected String mensaje() {
        return super.mensaje()+ "una moto y mi cilindrada es: "+ this.getCilindrada(); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    //Getters y Setters
    public int getCilindrada() {
        return cilindrada;
    }

    public void setCilindrada(int cilindrada) {
        this.cilindrada = cilindrada;
    }

    public String getMaterialManubrio() {
        return materialManubrio;
    }

    public void setMaterialManubrio(String materialManubrio) {
        this.materialManubrio = materialManubrio;
    }
    
    
    
}
