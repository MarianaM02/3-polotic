
package Logica;

public class Vehiculo {
    private String patente;
    private String chasis;
    private String motor;
    private String color;
    private String marca;
    private String modelo;
    private int cantAsientos;

    public Vehiculo(String patente, String chasis,String motor,String color,String marca,String modelo,int cantAsientos) {
        this.patente = patente;
        this.chasis = chasis;
        this.motor = motor;
        this.color = color;
        this.marca = marca;
        this.modelo = modelo;
        this.cantAsientos = cantAsientos;
    }
    
    protected String mensaje(){
        return "Hola soy ";
    }
    
    //Getters y Setters
    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCantAsientos() {
        return cantAsientos;
    }

    public void setCantAsientos(int cantAsientos) {
        this.cantAsientos = cantAsientos;
    }
    
    
}
