
package Persistencia;

import Logica.Alumno;
import Persistencia.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControladoraPersistencia {
    
    AlumnoJpaController alumnoJPA = new AlumnoJpaController();
    
    public void crearAlumno (Alumno alu){
        try {
            alumnoJPA.create(alu);
        } catch (Exception ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void eliminarAlumno(String aluID){
        try {
            alumnoJPA.destroy(aluID);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void modificarAlumno(Alumno alu){
        try {
            alumnoJPA.edit(alu);
        } catch (Exception ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Alumno> traerAlumnos() {
       
        List <Alumno> listaAlumnos = alumnoJPA.findAlumnoEntities();
        return listaAlumnos;
    }

    public Alumno traerUnAlumno(String id) {
        Alumno alu = alumnoJPA.findAlumno(id);
        return alu;
    }
}
