
package Logica;
import Persistencia.ControladoraPersistencia;
import java.util.Date;
import java.util.List;

public class TestJPA {

    public static void main(String[] args) {
       
        
        //Instancio un nuevo alumno
        //Alumno al = new Alumno("8448664", "Nicolas", "Menendez", new Date("08/31/1986"));
        
        //Creo referencia a la controladora
        ControladoraPersistencia control = new ControladoraPersistencia();
        
        //Utilizo la controladora para crear una entrada con la instancia hecha anteriormente
        //control.crearAlumno(al);
        
        
        //Borrar Registro
        //control.eliminarAlumno("8448664");
        
        //Edicion Registro
        //al.setApellido("Menéndez");
        //control.modificarAlumno(al);
        
        //Buscar y Mostrar
        
        //Todos
        //Crear lista de la clase
        List <Alumno> listaAlumnos = control.traerAlumnos();
        //Acceder a la lista
        for(Alumno al : listaAlumnos){
            System.out.println(al.getDni() +" "+ al.getApellido() +" "+ al.getNombre());
        }
        
        //Uno en Particular
        Alumno al= control.traerUnAlumno("32553381");
        System.out.println(al.getDni() +" "+ al.getApellido() +" "+ al.getNombre());
    }
    
}
