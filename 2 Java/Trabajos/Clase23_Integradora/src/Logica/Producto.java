
package Logica;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Producto implements Serializable {
    
    @Id
    private int cod_producto;
    @Basic
    private String nombre;
    private int cant;
    private String marca;
    private String tipo;
    @Temporal(TemporalType.DATE)
    private Date vencimiento;

    public Producto() {
    }

    public Producto(int cod_producto, String nombre, int cant, String marca, String tipo, Date vencimiento) {
        this.cod_producto = cod_producto;
        this.nombre = nombre;
        this.cant = cant;
        this.marca = marca;
        this.tipo = tipo;
        this.vencimiento = vencimiento;
    }

    public int getCod_producto() {
        return cod_producto;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCant() {
        return cant;
    }

    public String getMarca() {
        return marca;
    }

    public String getTipo() {
        return tipo;
    }

    public Date getVencimiento() {
        return vencimiento;
    }

    public void setCod_producto(int cod_producto) {
        this.cod_producto = cod_producto;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setVencimiento(Date vencimiento) {
        this.vencimiento = vencimiento;
    }
    
}
