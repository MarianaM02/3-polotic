
package Logica;

import Persistencia.ControladoraPersistencia;
import java.util.Date;

public class ControladoraLogica {
    
    ControladoraPersistencia controlPersis = new ControladoraPersistencia();
    
    public void altaProducto(int cod_producto, String nombre, int cant, String marca, String tipo, Date vencimiento){
        Producto prod = new Producto();
        prod.setCod_producto(cod_producto);
        prod.setNombre(nombre);
        prod.setCant(cant);
        prod.setMarca(marca);
        prod.setTipo(tipo);
        prod.setVencimiento(vencimiento);
        
        controlPersis.altaProducto(prod);
        
    }
}
