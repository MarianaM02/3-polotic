
package Logica;

public class GuiaN7 {

    public static void main(String[] args) {
        
        //Ejercicio1
        Auto auto1 = new Auto("Blanco", "Renault", "Logan", "98dfg76", 5);
        Auto auto2 = new Auto("Blanco", "Volkswagen", "Voyage", "74hjh34", 5);
        Auto auto3 = new Auto("Azul", "Peugeot", "405", "87fjjh54", 5);
        //Auto auto4 = new Auto;
        
        System.out.println("Marca: "+auto1.getMarca()+", Modelo: "+auto1.getModelo()+" Patente: "+auto1.getPatente());
        System.out.println("Marca: "+auto2.getMarca()+", Modelo: "+auto2.getModelo()+" Patente: "+auto2.getPatente());
        System.out.println("Marca: "+auto3.getMarca()+", Modelo: "+auto3.getModelo()+" Patente: "+auto3.getPatente());
        //System.out.println("Marca: "+auto4.getMarca());
        
        //Ejercicio2
        Mueble silla= new Mueble("Silla", 1.20, 0.40, "Madera", 4);
        Mueble mesa= new Mueble("Mesa", 0.90, 2.10, "Vidrio", 1);
        Mueble escritorio= new Mueble("Escritorio", .80, 1.25, "Metal", 2);
        
        System.out.println("Nombre: "+ silla.getNombre()+", Material: " + silla.getMaterial());
        System.out.println("Nombre: "+ mesa.getNombre()+", Material: " + mesa.getMaterial());
        System.out.println("Nombre: "+ escritorio.getNombre()+", Material: " + escritorio.getMaterial());
        
        silla.setMaterial("Madera");
        mesa.setMaterial("Madera");
        escritorio.setMaterial("Madera");
        
        System.out.println("Nombre: "+ silla.getNombre()+", Material: " + silla.getMaterial());
        System.out.println("Nombre: "+ mesa.getNombre()+", Material: " + mesa.getMaterial());
        System.out.println("Nombre: "+ escritorio.getNombre()+", Material: " + escritorio.getMaterial());
        
        //Ejercicio3
        Mascota vector[]= new Mascota[5];
        Mascota mascota1= new Mascota("Dobby","perro","macho","rubio","largo","mestizo");
        Mascota mascota2= new Mascota("Chock","perro","macho","negro","corto","Rott");
        Mascota mascota3= new Mascota("Luna","perro","macho","rubio","largo","Ovejero");
        Mascota mascota4= new Mascota("Kity","gato","macho","gris","corto","mestizo");
        Mascota mascota5= new Mascota("Raffles","perro","macho","marron","corto","Beagle");
        
        vector[0]=mascota1; 
        vector[1]=mascota2; 
        vector[2]=mascota3; 
        vector[3]=mascota4; 
        vector[4]=mascota5;
        
        for (int i = 0; i < vector.length; i++) {
            System.out.println("Nombre: "+ vector[i].getNombre()+ ", Especie: "+vector[i].getEspecie()+ ", Sexo: "+vector[i].getSexo());  
        }
        
        mascota3.setNombre("Nala");
        mascota4.setNombre("Zuko");
        
        System.out.println("Nombre: "+ vector[2].getNombre()+ ", Especie: "+vector[2].getEspecie()+ ", Sexo: "+vector[2].getSexo());  
        System.out.println("Nombre: "+ vector[3].getNombre()+ ", Especie: "+vector[3].getEspecie()+ ", Sexo: "+vector[3].getSexo());  
        
        for (int i = 0; i < vector.length; i++) {
            if(vector[i].getEspecie().equals("perro")){
                System.out.println("Nombre: "+ vector[i].getNombre()+ ", Especie: "+vector[i].getEspecie()+ ", Sexo: "+vector[i].getSexo());
            }  
        }
    }
    
}
