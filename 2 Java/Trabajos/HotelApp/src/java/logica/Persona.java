
package logica;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class Persona {
    
    @Id
    protected int id;
    @Basic    
    protected String dni;
    protected String nombres;
    protected String apellidos;
    protected String direccionDomicilio;
    @Temporal(TemporalType.DATE)
    protected Date fechaNacimiento;
    
}
